package net.kaneka.planttech2.utilities;

public class PlantTechConstants
{
    public static final int SOLARFOCUS_TYPE = 0; 
    public static final int RANGEUPGRADE_TYPE = 1; 
    public static final int SPEEDUPGRADE_TYPE = 2; 
    public static final int UPGRADECHIP_TYPE = 3; 
}
