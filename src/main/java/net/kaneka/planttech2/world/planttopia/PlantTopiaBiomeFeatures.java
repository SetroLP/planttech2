package net.kaneka.planttech2.world.planttopia;

import net.minecraft.block.Blocks;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;

public class PlantTopiaBiomeFeatures {

	public static final SurfaceBuilderConfig GRASS_DIRT_DIRT_SURFACE = new SurfaceBuilderConfig(Blocks.GRASS_BLOCK.getDefaultState(), Blocks.DIRT.getDefaultState(), Blocks.DIRT.getDefaultState());
	
}
